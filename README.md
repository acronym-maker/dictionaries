# Dictionaries

## Description

The purpose of this project is to host a number of dictionaries listing the
authorized words that *AcronymMaker* can use as acronyms.

All the dictionaries stored in this repository are in *normalized form*, i.e.,
they only use ASCII characters.

## Available Dictionaries

This section gives an overview of the dictionaries stored in this repository.

### English Dictionaries

+ [short (3000 words)](english/short.txt), which is made of the words from
  the [Oxford 3000](https://www.oxfordlearnersdictionaries.com/wordlist/american_english/oxford3000/).
+ [long (58110 words)](english/long.txt), from
  [Mieliestronk](http://www.mieliestronk.com/wordlist.html).

### French Dictionaries

+ [short (22740 words)](french/short.txt), which is that proposed by
  [Freelang](https://www.freelang.com/dictionnaire/dic-francais.php).
+ [long (336531 words)](french/long.txt), which has been produced by
  [Christophe Pallier](http://www.pallier.org/liste-de-mots-francais.html).
