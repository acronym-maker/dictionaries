#!/usr/bin/env python3

##############################################################################
#                                                                            #
#   AcronymMaker - Create awesome acronyms for your projects!                #
#   Copyright (c) 2020 - Romain Wallon (romain.gael.wallon@gmail.com)        #
#                                                                            #
#   This program is free software: you can redistribute it and/or modify     #
#   it under the terms of the GNU General Public License as published by     #
#   the Free Software Foundation, either version 3 of the License, or        #
#   (at your option) any later version.                                      #
#                                                                            #
#   This program is distributed in the hope that it will be useful,          #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     #
#   See the GNU General Public License for more details.                     #
#                                                                            #
#   You should have received a copy of the GNU General Public License        #
#   along with this program.                                                 #
#   If not, see <https://www.gnu.org/licenses/>.                             #
#                                                                            #
##############################################################################


"""
This script provides a convenient tool for normalizing the dictionaries of
words to be used by AcronymMaker.
"""


from argparse import ArgumentParser
from typing import Any, Dict
from unicodedata import normalize


#############
# FUNCTIONS #
#############


def parse_arguments() -> Dict[str, Any]:
    """
    Parses the command line arguments.

    :return: The options given to the script.
    """
    parser = ArgumentParser(prog='normalize.py',
                            description='Normalization script for the dictionaries of AcronymMaker')

    # Registering the option used to set the encoding of the input dictionary.
    parser.add_argument('-e', '--input-encoding',
                        help='the encoding of the input dictionary to normalize',
                        default=['utf-8'], nargs=1)

    # Registering the option used to specify the path of the input dictionary.
    parser.add_argument('-d', '--input-dictionary',
                        help='the path of the input dictionary to normalize',
                        type=str,
                        required=True, nargs=1)

    # Registering the option used to specify whether to overwrite the input dictionary.
    parser.add_argument('-o', '--overwrite',
                        help='if set, the input dictionary will be overwritten',
                        action='store_true')

    return vars(parser.parse_args())


def normalize_file(encoding: str, input_file: str, output_file: str) -> None:
    """
    Normalizes an input dictionary.

    :param encoding: The encoding of the input dictionary.
    :param input_file: The path of the input dictionary to normalize.
    :param output_file: The path of the output file, in which the normalized dictionary
                        will be stored.
    """
    with open(input_file, encoding=encoding) as file:
        lines = file.readlines()

    with open(output_file, mode='w') as file:
        for line in lines:
            file.write(normalize_string(line))


def normalize_string(string: str) -> str:
    """
    Normalizes the given string.

    :param string: The string to normalize.

    :return: The normalized string, containing only valid ASCII characters.
    """
    normalized = normalize('NFD', string).encode('ascii', 'ignore').decode("utf-8")
    return str(normalized).lower()


########
# MAIN #
########


if __name__ == '__main__':
    arguments = parse_arguments()
    encoding = arguments['input_encoding'][0]
    input_file = arguments['input_dictionary'][0]
    output_file = input_file if arguments['overwrite'] else f'{input_file}.normalized'
    normalize_file(encoding, input_file, output_file)
