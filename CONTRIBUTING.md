# Contributing Guidelines

Contributions to this project are welcome!
You will find below useful information if you wish to contribute.

Contributions to this project consist most likely in adding new dictionaries,
either for a language that is already recognized or for a new one.
New dictionaries must be added as `*.txt` file and in the directory
corresponding to its language (you may need to create this directory if the
language is not recognized yet).

New dictionaries must be *normalized* to ensure that they work well with
*AcronymMaker*.
To this end, we provide a [Python script](script/normalize.py) that makes easier
the normalization process.
Simply execute the following command to normalize your dictionary:

```bash
$ ./script/normalize.py -e utf-8 -d my-awesome-dictionary.txt -o
```

This command will read the file `my-awesome-dictionary.txt` as an UTF-8 file.
This file must contain one word per line.
The script will then replace each word by its normalized form **in the same
file**.
If you do not want the script to overwrite your dictionary, you must remove
the `-o` flag from the command above.

Finally, do not forget to add the link to the newly added dictionaries in the
[`README.md`](README.md) file of the project, using the existing dictionaries
as examples.

Once you have performed the changes described above in your own fork of this
project, you may ask for a merge request.
